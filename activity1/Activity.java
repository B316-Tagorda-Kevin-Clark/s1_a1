package com.zuitt.activity1;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args){
        Scanner myObj0 = new Scanner(System.in);
        System.out.println("First Name:");
        String firstName = myObj0.nextLine();

        Scanner myObj1 = new Scanner(System.in);
        System.out.println("Last Name:");
        String lastName = myObj1.nextLine();

        Scanner myGrade1 = new Scanner(System.in);
        System.out.println("First Subject Grade:");
        double grade1 = new Double(myGrade1.nextLine());

        Scanner myGrade2 = new Scanner(System.in);
        System.out.println("Second Subject Grade:");
        double grade2 = new Double(myGrade2.nextLine());

        Scanner myGrade3 = new Scanner(System.in);
        System.out.println("Third Subject Grade:");
        double grade3 = new Double(myGrade3.nextLine());

        double average = (grade1 + grade2 + grade3) / 3;

        System.out.println("Good day! " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + average);
    }
}


